## spriteCloud-assignment
## Overview
This repository contains the code for the spriteCloud assignment, which is a set of test for an website http://www.uitestingplayground.com/ and an API https://petstore.swagger.io/#/ . The test are written in Java using Selenium for UI testing and REST-assured for API testing. The goal of the project is to demonstrate the ability to automate test for a web-application and API in a CI/CD pipeline.

## Requirements
Java<br>
Maven<br>
Selenium<br>
REST-assured
## Running the Tests Locally
To run the tests locally, navigate to the root directory of the repository and run the following command:

```java
mvn clean test
```

This command will run all the tests located in the src/test directory and display the results in the console.

## Running the Tests in a CI/CD Pipeline

CI/CD integration was provided via gitlab with .gitlab.yml. I have integrated a pipeline that will run as two different stage APIs and UIs in Gitlab. At the end of both jobs, the results can be uploaded to the Calliope platform automatically.
You can run pipeline with manually on the gitlab.
The results of the tests can be viewed on Calliope.pro under the project dashboard.

## Link to the results in Calliope.pro
https://app.calliope.pro/profiles/4627/reports

## Improvement Point
One improvement point for the Calliope.pro platform would be to have better graph for test results. When I select only duration and failed test on the chart, even though the failed test is 0, the graphic range is between -1 and 1, this is an incorrect calculation. It should not be below 0 in any way.
## New Feature
A new feature that would be beneficial for the Calliope.pro platform would be to have the capability of integrate the other CI/CD platforms.

## Scenario Selection
For API, I wanted to do CRUD operations in the Pets Store. Using 3 interrelated APIs, I gathered them under a single test in the form of adding, updating and finding a new pet.
For UI cases, there may be a delay problem on most sites, and we may have to wait for the elements, so I wrote the Client Side Delay test. I wrote the Text Input test because a change is made on a different element when an action is taken. Last but not least, I wrote the login scenario found on almost all sites and is the most used by users.
In general, I wanted to show my skills in complex and always used operations.

## Most Important Scenarios
These selected scenarios are crucial in ensuring that the application is working correctly and that users can complete the most common tasks.

## Next Steps
The next steps for this project would be to expand the scope of the tests to include more edge cases, testing on different environments. Also, I will explore the possibility of integrating Selenium Grid. Finally, I will also work to improve the integration with test automation frameworks to obtain more detailed test results.
