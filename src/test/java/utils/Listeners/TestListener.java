package utils.Listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import lombok.SneakyThrows;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.TestRunner;
import ui.base.UiBaseTest;
import utils.ExtentReports.ExtentManager;
import utils.Log;



public class TestListener extends UiBaseTest implements ITestListener {

    private static final ExtentReports extent = ExtentManager.createInstance();
    public static final ThreadLocal<ExtentTest> test = new ThreadLocal<>();

    @Override
    public synchronized void onStart(ITestContext context) {
        Log.startLog("Test Suite started!");
        TestRunner runner = (TestRunner) context;
        String path = System.getProperty("user.dir");
        runner.setOutputDirectory(path + "/TestReports");
    }

    @Override
    public synchronized void onFinish(ITestContext context) {
        extent.flush();
    }

    @SneakyThrows
    @Override
    public synchronized void onTestStart(ITestResult result) {
        ExtentTest extentTest = extent.createTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
        test.set(extentTest);
        Log.startLog(result.getMethod().getMethodName() + " started!");
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        test.get().pass("Test passed");
    }

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        String caseName = result.getMethod().getMethodName();
        Log.info(caseName + " failed!");
        Object currentClass = result.getInstance();
        WebDriver webDriver = ((UiBaseTest) currentClass).getDriver();
        String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) webDriver)
                .getScreenshotAs(OutputType.BASE64);
        test.get().fail(result.getThrowable().toString(), MediaEntityBuilder
                .createScreenCaptureFromBase64String(base64Screenshot).build());
    }

    @Override
    public synchronized void onTestSkipped(ITestResult result) {
        Log.info(result.getMethod().getMethodName() + " skipped!");
        test.get().skip(result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        Log.info("onTestFailedButWithinSuccessPercentage for " + result.getMethod().getMethodName());
    }
}