package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.Listeners.TestListener;

public class Log {
    private static Logger Log = LogManager.getLogger(Log.class.getName());

    public static void startLog(String testClassName) {
        Log.info(testClassName + " Tests are Starting...");
    }

    public static void endLog(String testClassName) {
        Log.info(testClassName + " Tests are Ending...");
    }

    public static void info(String message) {
        Log.info("----- " + message + " -----");
        if (TestListener.test != null) {
            if (TestListener.test.get() != null) {
                TestListener.test.get().info(message);
            }
        }
    }
}
