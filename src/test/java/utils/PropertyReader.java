package utils;

import api.constants.ProjectType;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    Properties properties;

    public Properties getProperties(ProjectType projectType) {
        properties = new Properties();
        InputStream configFile = null;
        if (projectType.equals(ProjectType.API)) {
            try {
                configFile = getClass().getClassLoader().getResourceAsStream("api/config/api.properties");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                properties.load(configFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (projectType.equals(ProjectType.UI)) {
            try {
                configFile = getClass().getClassLoader().getResourceAsStream("ui/config/ui.properties");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                properties.load(configFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }

}
