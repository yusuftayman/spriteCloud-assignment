package utils.ExtentReports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.Platform;
import utils.Log;

import java.io.File;


public class ExtentManager {
    private static ExtentReports extent;
    private static Platform platform;
    private static final String reportFileName = "Test-Automation-Report.html";
    private static final String macPath = System.getProperty("user.dir") + "/TestReports";
    private static final String windowsPath = System.getProperty("user.dir") + "\\TestReports";
    private static final String linuxPath = System.getProperty("user.dir") + "/TestReports/";
    private static final String macReportFileLoc = macPath + "/" + reportFileName;
    private static final String winReportFileLoc = windowsPath + "\\" + reportFileName;
    private static final String linuxReportFileLoc = linuxPath + "/" + reportFileName;

    public static ExtentReports createInstance() {
        platform = getCurrentPlatform();
        String fileLocation = getReportFileLocation(platform);
        ExtentSparkReporter sparkReporter = new ExtentSparkReporter(fileLocation);
        sparkReporter.config().setTheme(Theme.DARK);
        sparkReporter.config().setDocumentTitle("Test Automation");
        sparkReporter.config().setReportName("spriteCloud TEST AUTOMATION");
        sparkReporter.config().setEncoding("utf-8");
        extent = new ExtentReports();
        extent.attachReporter(sparkReporter);

        return extent;
    }

    private static String getReportFileLocation(Platform platform) {
        String reportFileLocation = null;
        switch (platform) {
            case MAC:
                reportFileLocation = macReportFileLoc;
                createReportPath(macPath);
                Log.info("ExtentReport Path for MAC: " + macPath + "\n");
                break;
            case WINDOWS:
                reportFileLocation = winReportFileLoc;
                createReportPath(windowsPath);
                Log.info("ExtentReport Path for WINDOWS: " + windowsPath + "\n");
                break;
            case LINUX:
                reportFileLocation = linuxReportFileLoc;
                Log.info("Extent Report Path for Linux: " + linuxPath + "\n");
                break;
            default:
                Log.info("ExtentReport path has not been set! There is a problem!\n");
                break;
        }
        return reportFileLocation;
    }

    private static void createReportPath(String path) {
        File testDirectory = new File(path);
        if (!testDirectory.exists()) {
            if (testDirectory.mkdir()) {
                Log.info("Directory: " + path + " is created!");
            } else {
                Log.info("Failed to create directory: " + path);
            }
        } else {
            Log.info("Directory already exists: " + path);
        }
    }

    private static Platform getCurrentPlatform() {
        if (platform == null) {
            String os = System.getProperty("os.name").toLowerCase();
            if (os.contains("win")) {
                platform = Platform.WINDOWS;
            } else if (os.contains("nix") || os.contains("nux")
                    || os.contains("aix")) {
                platform = Platform.LINUX;
            } else if (os.contains("mac")) {
                platform = Platform.MAC;
            }
        }
        return platform;
    }
}