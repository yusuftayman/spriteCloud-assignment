package api.utils.responseDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Category {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
}
