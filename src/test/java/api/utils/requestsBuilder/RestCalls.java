package api.utils.requestsBuilder;

import api.base.ApiBaseTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.Log;

import java.util.Map;


public class RestCalls extends ApiBaseTest {

    public Response post(final String path, final Object inputEntity
            , Map<String, String> queryParams, Map<String, String> authMap, Map<String, String> formParams, Map<String, String> requestHeaderParams, final ContentType parameterContentType) {
        RequestSpecification httpRequest = RestAssured.given();
        if (authMap != null) {
            httpRequest.auth().basic(authMap.get("username"), authMap.get("password"));
        }
        if (formParams != null) {
            httpRequest.formParams(formParams);
        }
        if (queryParams != null) {
            httpRequest.queryParams(queryParams);
        }
        if (inputEntity != null) {
            httpRequest.body(inputEntity);
        }

        if (requestHeaderParams != null) {
            requestHeaderParams.put("Content-type", "application/json; charset=UTF-8");
            httpRequest.headers(requestHeaderParams);
        }

        return httpRequest.contentType(parameterContentType).when().post(path).then().assertThat().log().all().extract().response();
    }

    public Response get(final String path, Map<String, String> queryParams, final ContentType parameterContentType, Map<String, String> authMap, Map<String, String> requestHeaderParams) {
        RequestSpecification httpRequest = RestAssured.given();
        if (authMap != null) {
            httpRequest.auth().basic(authMap.get("username"), authMap.get("password"));
        }
        if (queryParams != null) {
            httpRequest.queryParams(queryParams);
        }
        if (requestHeaderParams != null) {
            Log.info("X-REQUESTED-WITH: " + requestHeaderParams.get("X-REQUESTED-WITH"));
            httpRequest.headers(requestHeaderParams);
        }

        return httpRequest.contentType(parameterContentType).when().get(path).then().assertThat().log().body().extract().response();
    }

    public Response put(final String path, final Object inputEntity, Map<String, String> queryParams, Map<String, String> authMap, Map<String, String> formParams, Map<String, String> requestHeaderParams, final ContentType parameterContentType) {
        RequestSpecification httpRequest = RestAssured.given();
        if (authMap != null) {
            httpRequest.auth().basic(authMap.get("username"), authMap.get("password"));
        }
        if (formParams != null) {
            httpRequest.formParams(formParams);
        }
        if (queryParams != null) {
            httpRequest.queryParams(queryParams);
        }
        if (inputEntity != null) {
            httpRequest.body(inputEntity);
        }
        if (requestHeaderParams != null) {
            requestHeaderParams.put("Content-type", "application/json; charset=UTF-8");
            Log.info("X-REQUESTED-WITH: " + requestHeaderParams.get("X-REQUESTED-WITH"));
            httpRequest.headers(requestHeaderParams);
        }
        return httpRequest.contentType(parameterContentType).when().put(path).then().assertThat().log().all().extract().response();
    }

    public Response delete(String path, Map<String, String> queryParams, Map<String, String> authMap, Map<String, String> requestHeaderParams, final ContentType parameterContentType) {
        RequestSpecification httpRequest = RestAssured.given();
        if (authMap != null) {
            httpRequest.auth().basic(authMap.get("username"), authMap.get("password"));
        }
        if (queryParams != null) {
            httpRequest.queryParams(queryParams);
        }
        if (requestHeaderParams != null) {
            requestHeaderParams.put("Content-type", "application/json; charset=UTF-8");
            Log.info("X-REQUESTED-WITH: " + requestHeaderParams.get("X-REQUESTED-WITH"));
            httpRequest.headers(requestHeaderParams);
        }
        return httpRequest.headers("Accept", ContentType.JSON).contentType(parameterContentType).when().delete(path).then().assertThat().log().all().extract().response();
    }
}
