package api.utils.requestDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class NewPetsRequestDto {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("category")
    private Category category;
    @JsonProperty("name")
    private String name;
    @JsonProperty("photoUrls")
    private List<String> photoUrls = null;
    @JsonProperty("tags")
    private List<Tag> tags = null;
    @JsonProperty("status")
    private String status;

    public NewPetsRequestDto newPetsRequestDto(Integer id, Category category, String name, List<String> photoUrls, List<Tag> tags, String status) {
        NewPetsRequestDto newPetsRequestDto = new NewPetsRequestDto();
        newPetsRequestDto.setId(id);
        newPetsRequestDto.setCategory(category);
        newPetsRequestDto.setName(name);
        newPetsRequestDto.setPhotoUrls(photoUrls);
        newPetsRequestDto.setTags(tags);
        newPetsRequestDto.setStatus(status);
        return newPetsRequestDto;
    }
}
