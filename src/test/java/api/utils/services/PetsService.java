package api.utils.services;

import api.utils.requestDto.NewPetsRequestDto;
import api.utils.requestsBuilder.RestCalls;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import utils.Log;

public class PetsService extends RestCalls {
    private static final String PETS = "/pet";

    /**
     * Add new pets to the store
     *
     * @param newPetRequestDto - new pet request dto
     *
     *
     */
    public Response addNewPet(NewPetsRequestDto newPetRequestDto) {
        Log.info("Add new pets to the store");
        return post(PETS, newPetRequestDto, null, null, null, null, ContentType.JSON);
    }

    /**
     * Update an existing pet
     *
     * @param newPetRequestDto - update pet request dto
     *
     *
     */
    public Response updatePet(NewPetsRequestDto newPetRequestDto) {
        Log.info("Update an existing pet");
        return put(PETS, newPetRequestDto, null, null, null, null, ContentType.JSON);
    }

    /**
     * Find pet by ID
     *
     * @param petId - ID of pet to return
     *
     *
     */
    public Response findPetById(String petId) {
        Log.info("Find pet by ID: " + petId);
        return get(PETS + "/" + petId, null, ContentType.JSON, null, null);
    }
}
