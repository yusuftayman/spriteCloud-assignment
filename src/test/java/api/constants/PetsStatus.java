package api.constants;

public enum PetsStatus {
    AVAILABLE("available"),
    PENDING("pending"),
    SOLD("sold");

    public final String status;

    PetsStatus(String status) {
        this.status = status;
    }
}
