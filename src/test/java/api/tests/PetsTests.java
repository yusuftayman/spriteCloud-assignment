package api.tests;

import api.base.ApiBaseTest;
import api.constants.PetsStatus;
import api.utils.requestDto.Category;
import api.utils.requestDto.NewPetsRequestDto;
import api.utils.requestDto.Tag;
import api.utils.responseDto.NewPetsResponseDto;
import api.utils.services.PetsService;
import io.restassured.response.Response;
import net.datafaker.Faker;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.Listeners.TestListener;
import utils.Log;

import java.util.ArrayList;
import java.util.List;

@Listeners({TestListener.class})
public class PetsTests extends ApiBaseTest {

    Integer petId;
    String petName;
    String petStatus;
    Faker faker;
    Category category;
    List<String> photoUrls;
    Tag tag;
    List<Tag> tags;
    NewPetsRequestDto newPetsRequestDto;
    PetsService petsService;

    @BeforeMethod(alwaysRun = true)
    public void before() {
        petsService = new PetsService();
        faker = new Faker();
        petId = faker.number().numberBetween(1,100);
        petName = faker.animal().name();
        petStatus = PetsStatus.AVAILABLE.status;
        category = new Category();
        category.setId(faker.number().numberBetween(1,100));
        category.setName(faker.name().firstName());
        photoUrls = new ArrayList<>();
        photoUrls.add(faker.internet().url());
        tag = new Tag();
        tags = new ArrayList<>();
        tag.setId(faker.number().numberBetween(1,100));
        tag.setName(faker.name().firstName());
        tags.add(tag);
        newPetsRequestDto = new NewPetsRequestDto();
    }

    @Test
    public void addNewPet() {
        newPetsRequestDto = newPetsRequestDto.newPetsRequestDto(petId, category, petName, photoUrls, tags, petStatus);

        Response addNewPetResponse = petsService.addNewPet(newPetsRequestDto);
        Assert.assertEquals(addNewPetResponse.getStatusCode(), HttpStatus.SC_OK, "The pet was not added successfully");

        NewPetsResponseDto newPetsResponseDto = addNewPetResponse.as(NewPetsResponseDto.class);
        Assert.assertEquals(newPetsResponseDto.getId(), petId, "The pet id is not correct");
        Assert.assertEquals(newPetsResponseDto.getName(), petName, "The pet name is not correct");
        Assert.assertEquals(newPetsResponseDto.getStatus(), petStatus, "The pet status is not correct");
        Log.info("The pet was added successfully");
    }

    @Test
    public void getPetById() {
        newPetsRequestDto = newPetsRequestDto.newPetsRequestDto(petId, category, petName, photoUrls, tags, petStatus);

        Response addNewPetResponse = petsService.addNewPet(newPetsRequestDto);
        Assert.assertEquals(addNewPetResponse.getStatusCode(), HttpStatus.SC_OK, "The pet was not added successfully");

        Response getPetByIdResponse = petsService.findPetById(petId.toString());
        Assert.assertEquals(getPetByIdResponse.getStatusCode(), HttpStatus.SC_OK, "The pet was not found");

        NewPetsResponseDto newPetsResponseDto = getPetByIdResponse.as(NewPetsResponseDto.class);
        Assert.assertEquals(newPetsResponseDto.getId(), petId, "The pet id is not correct");
        Assert.assertEquals(newPetsResponseDto.getName(), petName, "The pet name is not correct");
        Assert.assertEquals(newPetsResponseDto.getStatus(), petStatus, "The pet status is not correct");
        Log.info("The pet was found successfully");
    }

    @Test
    public void updatePet() {
        newPetsRequestDto = newPetsRequestDto.newPetsRequestDto(petId, category, petName, photoUrls, tags, petStatus);

        Response addNewPetResponse = petsService.addNewPet(newPetsRequestDto);
        Assert.assertEquals(addNewPetResponse.getStatusCode(), HttpStatus.SC_OK, "The pet was not added successfully");

        petStatus = PetsStatus.SOLD.status;
        newPetsRequestDto = newPetsRequestDto.newPetsRequestDto(petId, category, petName, photoUrls, tags, petStatus);

        Response updatePetResponse = petsService.updatePet(newPetsRequestDto);
        Assert.assertEquals(updatePetResponse.getStatusCode(), HttpStatus.SC_OK, "The pet was not updated successfully");

        NewPetsResponseDto newPetsResponseDto = updatePetResponse.as(NewPetsResponseDto.class);
        Assert.assertEquals(newPetsResponseDto.getId(), petId, "The pet id is not correct");
        Assert.assertEquals(newPetsResponseDto.getName(), petName, "The pet name is not correct");
        Assert.assertEquals(newPetsResponseDto.getStatus(), petStatus, "The pet status is not correct");
        Log.info("The pet was updated successfully");
    }

}
