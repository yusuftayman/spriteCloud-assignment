package api.base;

import api.constants.ProjectType;
import io.restassured.RestAssured;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utils.Log;
import utils.PropertyReader;

public class ApiBaseTest extends PropertyReader {

    @BeforeMethod
    public void setup() {
        RestAssured.baseURI = getProperties(ProjectType.API).getProperty("baseUri");
    }

    @AfterMethod
    public void tearDown() {
        Log.info("RestAssured Tests Completed");
    }

}
