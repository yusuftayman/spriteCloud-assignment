package ui.utils.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import utils.PropertyReader;

public class Browser extends PropertyReader {
    public static WebDriver driver;
    public String mainTab;
    public static Actions act = null;

    public WebDriver getDriver() {
        if (!isDriverOpened()) {
            createDriver();
            setMainTab();
        }
        return driver;
    }

    public void setMainTab() {
        mainTab = driver.getWindowHandle();
    }

    private static boolean isDriverOpened() {
        return driver != null;
    }

    public WebDriver createDriver() throws WebDriverException {
        driver = new ChromeDriver(getOptions());
        driver.manage().window().maximize();
        act = new Actions(driver);
        return driver;
    }

    public ChromeOptions getOptions() {
        ChromeOptions options = new ChromeOptions();
        options.setAcceptInsecureCerts(true);
        options.setHeadless(true);
        return options;
    }

    public static void quitDriver() {
        if (driver != null) {
            driver.quit();
        }
    }
}
