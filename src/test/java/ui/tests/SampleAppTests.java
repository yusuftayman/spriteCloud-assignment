package ui.tests;


import api.constants.ProjectType;
import net.datafaker.Faker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ui.base.UiBaseTest;
import ui.pages.HomePage;
import utils.Listeners.TestListener;

@Listeners({TestListener.class})
public class SampleAppTests extends UiBaseTest {
    String username;
    String password;
    String successText;
    Faker faker;

    @BeforeMethod(alwaysRun = true)
    public void before() {
        faker = new Faker();
        username = faker.name().firstName();
        password = getProperties(ProjectType.UI).getProperty("password");
        successText = "Welcome, " + username + "!";
    }

    @Test
    void loginTests() {
        createPage(HomePage.class)
                .goToSampleAppPage()
                .setUsername(username)
                .setPassword(password)
                .clickLoginButton()
                .checkSuccessText(successText);
    }
}
