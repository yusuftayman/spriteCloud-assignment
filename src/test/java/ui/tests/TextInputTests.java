package ui.tests;


import net.datafaker.Faker;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ui.base.UiBaseTest;
import ui.pages.HomePage;
import utils.Listeners.TestListener;

@Listeners({TestListener.class})
public class TextInputTests extends UiBaseTest {
    String inputText;
    Faker faker;

    @BeforeMethod(alwaysRun = true)
    public void before() {
        faker = new Faker();
        inputText = faker.name().firstName();
    }

    @Test
    void checkButtonNameUpdated() {
        createPage(HomePage.class)
                .goToTextInputPage()
                .updateButtonName(inputText)
                .checkButtonName(inputText);
    }
}
