package ui.tests;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ui.base.UiBaseTest;
import ui.pages.HomePage;
import utils.Listeners.TestListener;

@Listeners({TestListener.class})
public class ClientSideDelayTests extends UiBaseTest {
    String successText;

    @BeforeMethod(alwaysRun = true)
    public void before() {
        successText = "Data calculated on the client side.";
    }

    @Test
    void checkSuccessMessageAfterClickButton() {
        createPage(HomePage.class)
                .goToClientSideDelayPage()
                .triggerButton()
                .waitLoadingIconDisappear()
                .checkSuccessTextIsDisplayed(successText);
    }
}
