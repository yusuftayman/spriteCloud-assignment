package ui.base;

import api.constants.ProjectType;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ui.utils.driver.Browser;

import java.time.Duration;

public class UiBaseTest extends Browser {
    protected <T> T createPage(Class<T> pageType) {
        return PageFactory.initElements(getDriver(), pageType);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        createDriver();
        driver.get(getProperties(ProjectType.UI).getProperty("url"));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        Browser.quitDriver();
    }
}
