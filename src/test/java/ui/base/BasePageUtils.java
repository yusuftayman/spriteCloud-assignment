package ui.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.time.Duration;

/**
 * The class contains all element actions.
 *
 */
public class BasePageUtils {
    protected WebDriver driver;
    public static int WAIT_ELEMENT_DURATION = 15;

    /**
     * This is constructor method.
     *
     * @param driver is  the web driver.
     */
    public BasePageUtils(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Clear the element like textbox.
     * Sends value to textbox.
     *
     * @param by    should be like By.id("...")
     * @param value should be string type.
     * @return the element.
     */
    public WebElement sendKeys(By by, String value) {
        waitForElement(driver, WAIT_ELEMENT_DURATION, by);
        WebElement element = getElementBy(by);
        element.clear();
        element.sendKeys(value);
        return element;
    }


    /**
     * Waits for element is clickable.
     * Clicks the element by locating elements you want.
     *
     * @param by are locating elements.
     */
    public void clickObjectBy(By by) {
        waitForElementVisibility(driver, WAIT_ELEMENT_DURATION, by);
        waitForElementClickable(driver, by, WAIT_ELEMENT_DURATION);
        driver.findElement(by).click();
    }

    /**
     * Gets the elements by locating elements using 'by'.
     *
     * @param by are locating elements as id, xPath, name, class name etc.
     * @return the elements using List<> type.
     */
    public WebElement getElementBy(By by) {
        waitForElement(driver, WAIT_ELEMENT_DURATION, by);
        return driver.findElement(by);
    }

    /**
     * If the element is display, returns true. Else returns false and shows exception message.
     *
     * @param by are locating elements as id, xPath, name, class name etc.
     * @return element's display status which is boolean type.
     */
    public boolean isElementPresentAndDisplay(By by) {
        boolean status = false;
        try {
            status = driver.findElement(by).isDisplayed();
        } catch (Exception e) {
            Log.info(by + " element is NOT Displayed..!");
        }
        return status;
    }

    /**
     * Waits for element visibility every 2 millisecond in given 'seconds' parameter.
     *
     * @param driver    is the web driver.
     * @param seconds   are the limits of waited element.
     * @param elementBy are locating elements as id, xPath, name, class name etc.
     */
    public void waitForElementVisibility(WebDriver driver, int seconds, By elementBy) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
    }

    /**
     * Waits for element invisible every 2 millisecond in given 'seconds' parameter.
     *
     * @param driver    is the web driver.
     * @param seconds   are the limits of waited element.
     * @param elementBy are locating elements as id, xPath, name, class name etc.
     */
    public void waitForElementInvisibility(WebDriver driver, int seconds, By elementBy) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(elementBy));
    }

    /**
     * Waits for element every 2 millisecond in given 'seconds' parameter.
     *
     * @param driver    is the web driver.
     * @param seconds   are the limits of waited element.
     * @param elementBy are locating elements as id, xPath, name, class name etc.
     */
    public void waitForElement(WebDriver driver, int seconds, By elementBy) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.presenceOfElementLocated(elementBy));
    }

    /**
     * Waits for element clickable every 2 millisecond in given 'seconds' parameter.
     *
     * @param driver    is the web driver.
     * @param seconds   are the limits of waited element.
     * @param elementBy are locating elements as id, xPath, name, class name etc.
     */
    public void waitForElementClickable(WebDriver driver, By elementBy, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.elementToBeClickable(elementBy));
    }
}