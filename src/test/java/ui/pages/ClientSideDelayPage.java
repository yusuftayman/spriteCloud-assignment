package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ui.base.BasePageUtils;
import utils.Log;

public class ClientSideDelayPage extends BasePageUtils {


    public By TRIGGER_BUTTON = By.xpath("//button[@id='ajaxButton']");
    public By LOADING_ICON = By.xpath("//i[@id='spinner']");
    public By SUCCESS_TEXT = By.xpath("//p[@class='bg-success']");

    /**
     * This is constructor method.
     *
     * @param driver is  the web driver.
     */
    public ClientSideDelayPage(WebDriver driver) {
        super(driver);
        check();
    }

    protected void check() {
        Assert.assertTrue(isElementPresentAndDisplay(TRIGGER_BUTTON), "The client side delay page is not loaded successfully");
    }

    /**
     * Trigger the button.
     * @return the client side delay page.
     */
    public ClientSideDelayPage triggerButton() {
        clickObjectBy(TRIGGER_BUTTON);
        Log.info("The button is triggered");
        return this;
    }

    /**
     * Wait the loading icon disappear
     * @return the client side delay page.
     */
    public ClientSideDelayPage waitLoadingIconDisappear() {
        waitForElementInvisibility(driver, WAIT_ELEMENT_DURATION, LOADING_ICON);
        Log.info("The loading icon is disappeared");
        return this;
    }

    /**
     * Check the success text is displayed
     * @param successText is the success text.
     * @return the client side delay page.
     */
    public void checkSuccessTextIsDisplayed(String successText) {
        Assert.assertEquals(getElementBy(SUCCESS_TEXT).getText(), successText, "The success text is not displayed");
        Log.info("The success text is displayed");
    }
}
