package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ui.base.BasePageUtils;
import utils.Log;

public class TextInputPage extends BasePageUtils {


    public By NEW_BUTTON_NAME_INPUT = By.xpath("//input[@id='newButtonName']");
    public By UPDATE_BUTTON = By.xpath("//button[@id='updatingButton']");

    /**
     * This is constructor method.
     *
     * @param driver is  the web driver.
     */
    public TextInputPage(WebDriver driver) {
        super(driver);
        check();
    }

    protected void check() {
        Assert.assertTrue(isElementPresentAndDisplay(NEW_BUTTON_NAME_INPUT), "The text input page is not loaded successfully");
        Assert.assertTrue(isElementPresentAndDisplay(UPDATE_BUTTON), "The text input page is not loaded successfully");
    }

    /**
     * Updates the button name.
     *
     * @param buttonName is the new button name.
     */
    public TextInputPage updateButtonName(String buttonName) {
        sendKeys(NEW_BUTTON_NAME_INPUT, buttonName);
        clickObjectBy(UPDATE_BUTTON);
        Log.info("The button name is updated");
        return this;
    }

    /**
     * Check button name is updated.
     *
     * @param buttonName is the new button name.
     */
    public void checkButtonName(String buttonName) {
        Assert.assertEquals(getElementBy(UPDATE_BUTTON).getText(), buttonName, "The button name is not updated");
        Log.info("The button name is updated");
    }
}
