package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ui.base.BasePageUtils;
import utils.Log;

public class HomePage extends BasePageUtils {


    public By TEXT_INPUT_LINK = By.xpath("//a[@href='/textinput']");
    public By SAMPLE_APP_LINK = By.xpath("//a[@href='/sampleapp']");
    public By CLIENT_SIDE_DELAY_LINK = By.xpath("//a[@href='/clientdelay']");

    /**
     * This is constructor method.
     *
     * @param driver is  the web driver.
     */
    public HomePage(WebDriver driver) {
        super(driver);
        check();
    }

    protected void check() {
        Assert.assertTrue(isElementPresentAndDisplay(TEXT_INPUT_LINK), "The home page is not loaded successfully");
        Assert.assertTrue(isElementPresentAndDisplay(SAMPLE_APP_LINK), "The home page is not loaded successfully");
        Assert.assertTrue(isElementPresentAndDisplay(CLIENT_SIDE_DELAY_LINK), "The home page is not loaded successfully");
    }

    /**
     * Goes to the text input page.
     * @return the text input page.
     */
    public TextInputPage goToTextInputPage() {
        clickObjectBy(TEXT_INPUT_LINK);
        Log.info("The text input page is opened");
        return new TextInputPage(driver);
    }

    /**
     * Goes to the sample app page.
     * @return the sample app page.
     */
    public SampleAppPage goToSampleAppPage() {
        clickObjectBy(SAMPLE_APP_LINK);
        Log.info("The sample app page is opened");
        return new SampleAppPage(driver);
    }

    /**
     * Goes to the client side delay page.
     * @return the client side delay page.
     */
    public ClientSideDelayPage goToClientSideDelayPage() {
        clickObjectBy(CLIENT_SIDE_DELAY_LINK);
        Log.info("The client side delay page is opened");
        return new ClientSideDelayPage(driver);
    }
}
