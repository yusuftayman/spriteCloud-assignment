package ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ui.base.BasePageUtils;
import utils.Log;

public class SampleAppPage extends BasePageUtils {

    public By USERNAME_INPUT = By.xpath("//input[@name='UserName']");
    public By PASSWORD_INPUT = By.xpath("//input[@name='Password']");
    public By LOGIN_BUTTON = By.xpath("//button[@id='login']");
    public By SUCCESS_TEXT = By.xpath("//label[@id='loginstatus']");


    /**
     * This is constructor method.
     *
     * @param driver is  the web driver.
     */
    public SampleAppPage(WebDriver driver) {
        super(driver);
        check();
    }

    protected void check() {
        Assert.assertTrue(isElementPresentAndDisplay(USERNAME_INPUT), "The sample app page is not loaded successfully");
        Assert.assertTrue(isElementPresentAndDisplay(PASSWORD_INPUT), "The sample app page is not loaded successfully");
        Assert.assertTrue(isElementPresentAndDisplay(LOGIN_BUTTON), "The sample app page is not loaded successfully");
    }

    /**
     * Set username.
     * @param username is the username.
     */
    public SampleAppPage setUsername(String username) {
        sendKeys(USERNAME_INPUT, username);
        Log.info("The username is set");
        return this;
    }

    /**
     * Set password.
     * @param password is the password.
     */
    public SampleAppPage setPassword(String password){
        sendKeys(PASSWORD_INPUT, password);
        Log.info("The password is set");
        return this;
    }

    /**
     * Click login button.
     * @return the sample app page.
     */
    public SampleAppPage clickLoginButton() {
        clickObjectBy(LOGIN_BUTTON);
        Log.info("The login button is clicked");
        return this;
    }

    /**
     * Check success text is matched.
     *
     * @param successText is the success text.
     */
    public void checkSuccessText(String successText) {
        Assert.assertEquals(getElementBy(SUCCESS_TEXT).getText(), successText, "The success text is not matched");
        Log.info("The success text is matched");
    }



}
